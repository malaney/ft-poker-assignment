<?php

namespace Commands;

use App\Deck;
use App\Hand;
use App\HandEvaluator;
use Faker\Factory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PlayPokerCommand extends Command
{
    protected static $defaultName = 'ft:play-poker';

    protected function configure()
    {
        $this
            ->setDescription('Simulates a poker tournament.')
            ->addArgument('numPlayers', InputArgument::OPTIONAL, 'Number of players to deal in.', 4)
            ->addArgument('exitCondition',
                InputArgument::OPTIONAL,
                'Number from 2-10 which tells simulation when to stop.  2 is pair, 10 is royal flush.',
                5
            )
            ->setHelp('This command allows you to simulate poker games with a number of players');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $faker = Factory::create();

        $numPlayers = $input->getArgument('numPlayers');
        $exitCondition = $input->getArgument('exitCondition');

        if (($numPlayers < 2) || ($numPlayers > 10)) {
            $io->error("You must select between two (2) and ten (10) players");
            exit;
        }

        $players = array_map(function () use ($faker) {
            return $faker->name;
        }, array_fill(0, $numPlayers, ''));

        $numCards = 5;
        $numHands = 0;

        $exitConditions = [];
        $winnings = array_combine($players, array_fill(0, $numPlayers, 0));

        $io->title('FT Poker Tournament');
        while (array_sum($exitConditions) === 0) {
            // Initialize Deck
            $deck = new Deck();
            $deck->shuffle()->myShuffle()->cut();
            $hands = [];

            $io->section("Round {$numHands}");

            // Deal cards to each player
            for ($i = 0; $i < $numCards; $i++) {
                foreach ($players as $player) {
                    if (empty($hands[$player])) {
                        $hands[$player] = new Hand();
                    }
                    try {
                        $hands[$player]->addCard($deck->dealOne());
                    } catch (\Exception $e) {
                        $io->error('Sorry, the deck is out of cards!  Try reducing the number of players or the number of cards.');
                        exit;
                    }
                }
            }

            $points = $datum = $handDisplays = [];
            // Evaluate each players hand and compile data
            foreach ($players as $player) {
                /** @var Hand $hand */
                $hand = $hands[$player];
                $evaluator = new HandEvaluator($hand);
                $points[$player] = $pts = $evaluator->evaluate();

                $datum[] = [
                    'player'         => $player,
                    'high card'      => (string) $hand->getHighCard(),
                    'straight'       => $hand->hasStraight() ? 'Y' : 'N',
                    'flush'          => $hand->hasFlush() ? 'Y' : 'N',
                    '2 of a kind'    => $hand->hasTwoOfAKind() ? 'Y' : 'N',
                    '2 pair'         => $hand->hasTwoPair() ? 'Y' : 'N',
                    '3 of a kind'    => $hand->hasThreeOfAKind() ? 'Y' : 'N',
                    '4 of a kind'    => $hand->hasFourOfAKind() ? 'Y' : 'N',
                    'full house'     => $hand->hasFullHouse() ? 'Y' : 'N',
                    'straight flush' => $hand->hasStraightFlush() ? 'Y' : 'N',
                    'royal flush'    => $hand->hasRoyalFlush() ? 'Y' : 'N',
                    'points'         => (new HandEvaluator($hand))->evaluate(),
                ];

                $handDisplays[] = [
                    'player' => $player,
                    'hand'   => $hand->display(),
                ];

                //
                // Specify an exit condition here to determine
                // when the program should break
                // (i.e. $pts > 10000, first player to score over 10000 pts)
                //
                $exitConditions[$player] = ($pts >= pow(10, $exitCondition)) ? 1 : 0;
            }

            $winner = array_search(max($points), $points);
            $winnings[$winner]++;
            $numHands++;

            $handTable = new Table($output);
            $handTable->setHeaders(['Player', 'Hand'])
                ->setRows($handDisplays)
                ->render();

            $dataTable = new Table($output);
            $dataTable->setHeaders(array_keys($datum[0]))
                ->setRows($datum)
                ->render();

            $io->success("$winner wins");
        }
        $io->note("It took $numHands hands to reach exit condition");

        // Compile and display stats
        $stats = [];
        foreach ($winnings as $winner => $numWins) {
            $stats[] = [
        'Player'   => $winner,
        'Num Wins' => $numWins,
        'Win %'    => round(($numWins / $numHands) * 100, 2),
    ];
        }

        $statsTable = new Table($output);
        $statsTable->setHeaders([
            [new TableCell('FT Poker Tournament Stats', ['colspan' => 3])],
            array_keys($stats[0]),
        ])
            ->setRows($stats)
            ->render();
    }
}
