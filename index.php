<?php

require 'vendor/autoload.php';

use Commands\PlayPokerCommand;
use Symfony\Component\Console\Application;

if (getenv('APP_ENV') == 'test') {
    return;
}

$application = new Application();
$application->add(new PlayPokerCommand());
$application->run();

