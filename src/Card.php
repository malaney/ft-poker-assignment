<?php

namespace App;

use Exception as ExceptionAlias;

class Card
{
    const SUIT_CLUBS = 'Clubs';
    const SUIT_DIAMONDS = 'Diamonds';
    const SUIT_HEARTS = 'Hearts';
    const SUIT_SPADES = 'Spades';

    public static $suits = [
        self::SUIT_CLUBS,
        self::SUIT_DIAMONDS,
        self::SUIT_HEARTS,
        self::SUIT_SPADES,
    ];

    public static $values = [
        'Deuce',
        'Trey',
        'Four',
        'Five',
        'Six',
        'Seven',
        'Eight',
        'Nine',
        'Ten',
        'Jack',
        'Queen',
        'King',
        'Ace'
    ];

    /** @var bool */
    private $isDealt = false;

    /** @var int */
    private $order = 0;

    /** @var string */
    private $suit;

    /** @var string */
    private $value;

    /**
     * Card constructor.
     * @param string $value
     * @param string $suit
     * @param int $order
     * @throws ExceptionAlias
     */
    public function __construct(string $value, string $suit, int $order = 0)
    {
        $this->setSuit($suit);
        $this->setValue($value);
        $this->order = $order;
    }

    /**
     * @return bool
     */
    public function isDealt(): bool
    {
        return $this->isDealt;
    }

    /**
     * @param bool $isDealt
     * @return Card
     */
    public function setIsDealt(bool $isDealt): self
    {
        $this->isDealt = $isDealt;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     * @return Card
     */
    public function setOrder(int $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return string
     */
    public function getSuit(): string
    {
        return $this->suit;
    }

    /**
     * @param string $suit
     * @return Card
     * @throws ExceptionAlias
     */
    public function setSuit(string $suit): self
    {
        if (in_array($suit, self::$suits)) {
            $this->suit = $suit;

            return $this;
        }

        throw new ExceptionAlias("Invalid suit [{$suit}]");
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    public function getPointValue()
    {
        return array_search($this->value, self::$values);
    }

    /**
     * @param string $value
     * @return Card
     * @throws ExceptionAlias
     */
    public function setValue(string $value): self
    {
        if (in_array($value, self::$values)) {
            $this->value = $value;

            return $this;
        }

        throw new ExceptionAlias ("Invalid value [{$value}]");
    }

    /**
     * @return string
     */
    public function display()
    {
        return (string) $this;
    }

    public function __toString()
    {
        return $this->getValue() . ' of ' . $this->getSuit();
    }
}
