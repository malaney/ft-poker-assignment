<?php

namespace App;

class Hand
{
    /** @var Card[] $cards */
    private $cards = [];

    public function __construct(array $cards = [])
    {
        $this->cards = $cards;
    }

    public function display()
    {
        return implode(', ', $this->cards);
   }

    public function addCard(Card $card)
    {
        $this->cards[] = $card;
    }

    public function removeCard(Card $card)
    {
        if (in_array($card, $this->cards)) {
            $key = array_search($card, $this->cards);
            unset($this->cards[$key]);
        }
    }

    public function sortByValue()
    {
        $cards = $this->getCards();
        usort($cards, function(Card $a, Card $b) {
            return $a->getPointValue() > $b->getPointValue();
        });
        $this->setCards($cards);
    }

    public function sortBySuit()
    {
        $cards = $this->getCards();
        usort($cards, function(Card $a, Card $b) {
            return $a->getSuit() > $b->getSuit();
        });
        $this->setCards($cards);
    }

    public function sortBySuitThenByValue()
    {
        $cards = $this->getCards();
        usort($cards, function(Card $a, Card $b) {
            return $a->getPointValue() <=> $b->getPointValue() ?: $a->getSuit() > $b->getSuit();
        });
        $this->setCards($cards);
    }

    public function sortByValueThenBySuit()
    {
        $cards = $this->getCards();
        usort($cards, function(Card $a, Card $b) {
            return $a->getSuit() <=> $b->getSuit() ?: $a->getPointValue() > $b->getPointValue();
        });
        $this->setCards($cards);
    }

    public function hasStraight(int $len = 5, bool $sameSuit = false)
    {
        if (count($this->cards) < $len) {
           return false;
        }

        if ($sameSuit) {
            $this->sortByValue();
            foreach ($this->getSuits() as $suit) {
                $maxConsecutiveCards = $this->getMaxConsecutiveCards($this->getCards($suit));
                if ($maxConsecutiveCards >= $len) {
                    return true;
                }
            }

            return false;
        } else {
            $this->sortByValue();
            return $this->getMaxConsecutiveCards($this->getCards()) >= $len;
        }
    }

    public function hasFlush(int $len = 5)
    {
        $suits = array_reduce($this->getCards(), function (array $accumulator, Card $card) {
            $accumulator[$card->getSuit()] += 1;

            return $accumulator;
        }, ['Clubs' => 0, 'Diamonds' => 0, 'Hearts' => 0, 'Spades' => 0]);

        return max($suits) >= $len;
    }

    public function hasTwoOfAKind()
    {
        return $this->hasNOfAKind(2);
    }


    public function hasThreeOfAKind()
    {
        return $this->hasNOfAKind(3);
    }

    public function hasFourOfAKind()
    {
        return $this->hasNOfAKind(4);
    }

    public function hasFullHouse()
    {
        return $this->hasTwoOfAKind() && $this->hasThreeOfAKind();
    }

    public function hasNOfAKind($n)
    {
       $this->sortByValue();

       $values = array_map(function(Card $card) {
           return $card->getValue();
       }, $this->getCards());

       $counts = array_count_values($values);

       return array_search($n, $counts);
    }

    /**
     * @return bool
     */
    public function hasTwoPair()
    {
        $this->sortByValue();

        $values = array_map(function(Card $card) {
            return $card->getValue();
        }, $this->getCards());

        $counts = array_count_values($values);
        $sum = array_reduce($counts, function ($carry, $count) {
            $carry += ($count === 2) ? 1 : 0;
            return $carry;
        }, 0);

        return $sum === 2;
    }

    public function hasStraightFlush()
    {
        $this->sortByValue();

        return $this->hasFlush() && $this->getMaxConsecutiveCards($this->getCards()) == 5;
    }

    public function hasRoyalFlush()
    {
        return $this->hasStraightFlush() && $this->getHighCardPointValue() == 12;
    }

    public function setCards(array $cards)
    {
       $this->cards = $cards;
    }

    public function getCards(string $suit = null)
    {
        if ($suit) {
           return array_filter($this->cards, function(Card $card) use ($suit) {
              return $card->getSuit() == $suit;
           });
        } else {
            return $this->cards;
        }
    }

    /**
     * @return array
     */
    private function getSuits()
    {
        $suits = array_reduce($this->getCards(), function (array $accumulator, Card $card) {
            $accumulator[$card->getSuit()][] = $card;

            return $accumulator;
        }, []);

        return array_keys($suits);
    }

    /**
     * @param array $cards
     * @return int
     */
    protected function getMaxConsecutiveCards(array $cards): int
    {
        /** @var null|Card $prevCard */
        $prevCard = null;
        $consecutiveArray = $result = [];
        /** @var Card $card */
        foreach ($cards as $card) {
            if (is_null($prevCard)) {
                $prevCard = $card;
                $consecutiveArray = [$card->getPointValue()];
            } else {
                if ($card->getPointValue() == $prevCard->getPointValue() + 1) {
                    $consecutiveArray[] = $card->getPointValue();
                } else {
                    $result[] = $consecutiveArray;
                    $consecutiveArray = [$card->getPointValue()];
                }
                $prevCard = $card;
            }
        }
        $result[] = $consecutiveArray;
        $count = array_map('count', $result);
        return max($count);
    }

    /**
     * @return Card
     * @throws \Exception
     */
    public function getHighCard()
    {
        $highCard = array_reduce($this->getCards(), function(Card $carry, Card $card) {
            if ($carry->getPointValue() > $card->getPointValue()) {
                return $carry;
            }
            return $card;
        },  new Card('Deuce', 'Spades'));

        return $highCard;
    }

    /**
     * @return false|int|string
     * @throws \Exception
     */
    public function getHighCardPointValue()
    {
        return $this->getHighCard()->getPointValue();
    }
}