<?php

namespace App;

class Deck
{
    /** @var Card[]  */
    private $cards = [];

    public function __construct()
    {
       $this->cards;

       $i = 0;
       foreach (Card::$suits as $suit) {
           foreach (Card::$values as $value) {
               array_push($this->cards, new Card($value, $suit, $i++));
           }
       }
    }

    public function display()
    {
        $dealt = $undealt = [];

        foreach ($this->cards as $card) {
            $dealt[] = ($card->isDealt()) ? $card : null;
            $undealt[] = ($card->isDealt()) ? null : $card;
        }

        $dealt = array_filter($dealt);
        $undealt = array_filter($undealt);

        if (count($dealt)) {
            print "Dealt\n" . implode("\n", $dealt);
        }

        if (count($undealt)) {
            print "\nUndealt\n" . implode("\n", $undealt);
        }
    }

    /**
     * @return Card
     * @throws \Exception
     */
    public function dealOne()
    {
       $card = array_pop($this->cards);
       if (!$card) {
           throw new \Exception('Deck is empty!!');
       }
       $card->setIsDealt(true);

       return $card;
    }

    public function getCards()
    {
        return $this->cards;
    }

    public function shuffle()
    {
        $cards = $this->getCards();
        shuffle($cards);
        $this->setCards($cards);

        return $this;
    }

    public function myShuffle()
    {
        $cards = $this->getCards();

        for ($i = 0; $i < count($cards); ++$i) {
            $r = rand(0, $i);
            $tmp = $cards[$i];
            $cards[$i] = $cards[$r];
            $cards[$r] = $tmp;
        }

        $this->setCards($cards);
        return $this;
    }

    public function simpleShuffle()
    {
        $cards = $this->getCards();

        for ($i = 0; $i < count($cards); ++$i) {
            $r = mt_rand(0, $i);
            list($cards[$i], $cards[$r]) = [$cards[$r], $cards[$i]];
        }

        $this->setCards($cards);
        return $this;
    }

    public function cut()
    {
        $cards = $this->getCards();
        $cutPoint = array_rand($cards);

        $firstHalf = array_slice($cards, 0, $cutPoint);
        $secondHalf = array_slice($cards, $cutPoint, count($cards));

        $result = array_merge($secondHalf, $firstHalf);
        $this->setCards($result);

        return $this;
    }

    private function setCards(array $cards)
    {
        $this->cards = $cards;
    }
}
