<?php

namespace App;

class HandEvaluator
{
    /** @var Hand  */
    private $hand;
    private $evaluations;

    public function __construct(Hand $hand)
    {
        $this->hand = $hand;
        // Basic high card value
        $this->addEvaluation(function (Hand $hand) {
            return $hand->getHighCardPointValue();
        });

        $this->addEvaluation(function (Hand $hand) {
            return ($hand->hasTwoOfAKind()) ? pow(10, 2) : 0;
        });

        $this->addEvaluation(function (Hand $hand) {
            return ($hand->hasTwoPair()) ? pow(10, 3) : 0;
        });

        $this->addEvaluation(function (Hand $hand) {
            return ($hand->hasThreeOfAKind() ? pow(10, 4) : 0);
        });

        $this->addEvaluation(function (Hand $hand) {
            return ($hand->hasStraight(5)) ? pow(10,5) : 0;
        });

        $this->addEvaluation(function (Hand $hand) {
            return ($hand->hasFlush()) ? pow(10, 6) : 0;
        });

        $this->addEvaluation(function (Hand $hand) {
            return ($hand->hasFullHouse()) ? pow(10, 7) : 0;
        });

        $this->addEvaluation(function (Hand $hand) {
            return ($hand->hasFourOfAKind()) ? pow(10, 8) : 0;
        });

        $this->addEvaluation(function (Hand $hand) {
            return ($hand->hasStraightFlush()) ? pow(10, 9) : 0;
        });

        $this->addEvaluation(function (Hand $hand) {
            return ($hand->hasRoyalFlush()) ? pow(10, 10) : 0;
        });
    }

    /**
     * @return Hand
     */
    public function getHand(): Hand
    {
        return $this->hand;
    }

    /**
     * @param Hand $hand
     */
    public function setHand(Hand $hand): void
    {
        $this->hand = $hand;
    }

    /**
     * @return mixed
     */
    public function getEvaluations()
    {
        return $this->evaluations;
    }

    public function addEvaluation($evaluation)
    {
        $this->evaluations[] = $evaluation;
    }

    /**
     * @return int
     */
    public function evaluate()
    {
        $result = array_reduce($this->getEvaluations(), function ($carry, $eval) {
            $carry += call_user_func($eval, $this->hand);
            return $carry;
        }, 0);

        return $result;
    }
}