<?php

namespace tests;

use App\Card;
use App\Hand;
use PHPUnit\Framework\TestCase;

class HandTest extends TestCase
{
    /** @var Hand */
    private $hand;

    public function setUp()
    {
        $this->hand = new Hand([
            new Card('King', 'Diamonds'),
            new Card('Ace', 'Spades'),
            new Card('Trey', 'Spades'),
            new Card('Queen', 'Diamonds'),
            new Card('Deuce', 'Spades'),
            new Card('Ace', 'Diamonds'),
            new Card('Ace', 'Clubs'),
        ]);
    }

    /** test */
    public function it_can_display_cards()
    {
        $this->assertTrue(method_exists($this->hand, 'display'));
        $this->assertEquals($this->hand->display(), "Ace of Spades,Deuce of Spades,Trey of Spades");
    }

    /** @test */
    public function it_can_receive_a_card()
    {
        $this->assertTrue(method_exists($this->hand, 'addCard'));
    }

    /** @test */
    public function it_can_determine_a_straight()
    {
        $this->assertTrue(method_exists($this->hand, 'hasStraight'));
        $this->assertTrue($this->hand->hasStraight(3));
        $this->assertFalse($this->hand->hasStraight(4));
        $this->assertTrue($this->hand->hasStraight(3, true));
    }

    /** @test */
    public function it_can_sort_by_value_then_by_suit()
    {
        $this->assertTrue(method_exists($this->hand, 'sortByValueThenBySuit'));
        $this->hand->sortByValueThenBySuit();
        $this->assertEquals($this->hand->display(), "Ace of Clubs, Queen of Diamonds, King of Diamonds, Ace of Diamonds, Deuce of Spades, Trey of Spades, Ace of Spades");
    }

    /** @test */
    public function it_can_sort_by_suit_then_by_value()
    {
        $this->assertTrue(method_exists($this->hand, 'sortBySuitThenByValue'));
        $this->hand->sortBySuitThenByValue();
        $this->assertEquals($this->hand->display(), "Deuce of Spades, Trey of Spades, Queen of Diamonds, King of Diamonds, Ace of Clubs, Ace of Diamonds, Ace of Spades");
    }

    /** @test */
    public function it_can_determine_n_of_a_kind()
    {
        $this->assertTrue(method_exists($this->hand, 'hasNOfAKind'));
        $this->assertNotEmpty($this->hand->hasNOfAKind(3));
    }

    /** @test */
    public function it_can_determine_a_flush()
    {
        $this->assertTrue(method_exists($this->hand, 'hasFlush'));
        $this->assertTrue($this->hand->hasFlush(3));
        $this->assertFalse($this->hand->hasFlush());

        $hand = new Hand([
            new Card('Jack', 'Spades'),
            new Card('King', 'Spades'),
            new Card('Ace', 'Spades'),
            new Card('Trey', 'Spades'),
            new Card('Deuce', 'Spades'),
        ]);

        $this->assertTrue($hand->hasFlush());
    }

    /** @test */
    public function it_can_determine_two_pair()
    {
        $this->assertTrue(method_exists($this->hand, 'hasTwoPair'));

        $hand = new Hand([
            new Card('Jack', 'Spades'),
            new Card('Jack', 'Hearts'),
            new Card('Ace', 'Spades'),
            new Card('Deuce', 'Clubs'),
            new Card('Deuce', 'Diamonds'),
        ]);

        $this->assertTrue($hand->hasTwoPair());
    }

    /** @test */
    public function it_can_determine_a_royal_flush()
    {
        $this->assertTrue(method_exists($this->hand, 'hasTwoPair'));

        $hand = new Hand([
            new Card('Ten', 'Spades'),
            new Card('Jack', 'Spades'),
            new Card('Queen', 'Spades'),
            new Card('King', 'Spades'),
            new Card('Ace', 'Spades'),
        ]);

        $this->assertTrue($hand->hasRoyalFlush());
    }

    /** @test */
    public function it_can_determine_the_highest_card()
    {
        $this->assertTrue(method_exists($this->hand, 'getHighCard'));

        $hand = new Hand([
            new Card('Ten', 'Spades'),
            new Card('Jack', 'Spades'),
            new Card('Queen', 'Spades'),
            new Card('King', 'Spades'),
            new Card('Trey', 'Spades'),
        ]);

        $this->assertEquals($hand->getHighCard()->getPointValue(), 11);
    }
}