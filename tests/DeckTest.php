<?php

namespace tests;

use App\Card;
use App\Deck;
use PHPUnit\Framework\TestCase;

class DeckTest extends TestCase
{
    /** @var Deck */
    private $deck;

    public function setUp()
    {
        $this->deck = new Deck();
    }

    /** @test */
    public function it_can_display_all_cards()
    {
        $this->assertTrue(method_exists($this->deck, 'display'));
    }

    /** @test */
    public function it_can_deal_one_card()
    {
        $this->assertTrue(method_exists($this->deck, 'dealOne'));
        $this->assertEquals(count($this->deck->getCards()), 52);
        $card = $this->deck->dealOne();
        $this->assertTrue($card instanceof Card);
    }

    /** @test */
    public function it_can_shuffle_its_cards()
    {
        $this->assertTrue(method_exists($this->deck, 'shuffle'));
        $this->deck->shuffle();
        $this->assertEquals(count($this->deck->getCards()), 52);
    }

    /** @test */
    public function it_can_cut_its_cards()
    {
        $this->assertTrue(method_exists($this->deck, 'cut'));
        $this->deck->shuffle();
        $this->assertEquals(count($this->deck->getCards()), 52);
    }

    /** @test */
    public function it_contains_52_cards()
    {
        $this->assertEquals(count($this->deck->getCards()), 52);
    }
}