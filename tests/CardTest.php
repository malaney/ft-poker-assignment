<?php

namespace tests;

use App\Card;
use PHPUnit\Framework\TestCase;

class CardTest extends TestCase
{
    /** @var Card */
    private $card;

    public function setUp()
    {
        $this->card = new Card('Ace', 'Spades');

    }

    /** @test */
    public function it_can_display_a_card()
    {
        $this->assertTrue(method_exists($this->card, 'display'));
        $this->assertEquals($this->card->display(), 'Ace of Spades');
    }
}