# FT Poker Programming Assignment

This code creates a CLI-based Poker Tournament for 2 or more players

## Installation

Installation assumes git and [composer](https://getcomposer.org) are already installed.

```sh
git clone <repo>
cd ft-poker-assignment
composer install
./vendor/bin/phpunit  # to run tests
```

## Usage

```sh
php index.php ft:play-poker [<numPlayers> [<exitCondition>]]
```

```sh
php index.php ft:play-poker --help # for help
```

## Features

* Initializes a deck of cards, shuffles them, cuts them and deals them out to players in a Hand.
* Displays each players hand, evaluates each hand and determines winner.
* Evaluations for n-of a kind, flush, straight, full house, straight flush and royal flush
* Able to run through a series of hands until an exit condition is met (e.g. full house is dealt)
* Several shuffle implementations
* Test coverage for Card, Deck and Hand classes
